<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Route::get('/', ['as' => 'index', 'uses' => 'AlbumsController@getList']);
Route::get('/createalbum', ['as' => 'create_album_form', 'uses' => 'AlbumsController@getForm']);
Route::post('/createalbum', ['as' => 'create_album', 'uses' => 'AlbumsController@postCreate']);
Route::get('/deletealbum/{id}', ['as' => 'delete_album', 'uses' => 'AlbumsController@getDelete']);
Route::get('/album/{id}', ['as' => 'show_album', 'uses' => 'AlbumsController@getAlbum']);

Route::get('/addimage/{id}', ['as' => 'add_image', 'uses' => 'ImageController@getForm']);
Route::post('/addimage', ['as' => 'add_image_to_album', 'uses' => 'ImageController@postAdd']);
Route::get('/deleteimage/{id}', ['as' => 'delete_image', 'uses' => 'ImageController@getDelete']);

Route::post('/moveimage', ['as' => 'move_image', 'uses' => 'ImageController@postMove']);

Route::get('/allImages', ['as' => 'allImages', 'uses' => 'AlbumsController@allAlbums']);
Route::get('/albums/{id}', ['as' => 'showSingleAlbum', 'uses' => 'AlbumsController@someAlbums']);
