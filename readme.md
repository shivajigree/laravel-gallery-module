

<p align="center">
<img src="https://visualpharm.com/assets/624/Gallery-595b40b65ba036ed117d4120.svg" width="400"></p>

<p align="center">
<a href=""><img src="https://img.shields.io/gitlab/pipeline/shivajigree/laravel-gallery-module?style=for-the-badge" alt="Build Status"></a>
</p>

## About LaraGallery

LaraGallery is a gallery module built with laravel v5.5. It has got an elegant look to showcase your photos. Its features includes

- [Individul Albums for photos]().
- [Any Number of Photos can be added in Albums. ]().
- [Illustrative slideshow included in the photos with multiple tools to download, share, zoom, etc]().
- [Supporting Full HD images]().

## Contributing

Sole contribution belongs to the developer.

## Security Vulnerabilities

If you discover a security vulnerability within this module, please send an e-mail to Shiva Kunwar at shiva.kunwar@hotmail.com. All security vulnerabilities will be promptly addressed.

## License

The LaraGallery is open-sourced software licensed under the [GNU GENERAL PUBLIC LICENSE](http://www.gnu.org/licenses).
